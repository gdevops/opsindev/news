
.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/documentation/tools/rss.xml>`_

.. _opsindev_news:

====================
OpsInDev news
====================

.. toctree::
   :maxdepth: 5


   articles/articles
