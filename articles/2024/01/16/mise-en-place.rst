.. index::
   pair: asdf; mise-en-place
   ! mise-en-place

.. _mise_en_place_2024_01_16:

======================================================
2024-01-16 **mise-en-place (formerly called "rtx")**
======================================================

- https://github.com/jdx
- https://github.com/jdx/mise
- https://mise.jdx.dev/
- https://mise.jdx.dev/dev-tools/
- https://mise.jdx.dev/environments.html


jdx
====

author of mise-en-place, oclif, and heroku/cli

Description
=============

The front-end to your dev env (formerly called "rtx")

Coming from rtx
===================

mise was formerly called rtx.

The name was changed to avoid confusion with Nvidia's line of graphics cards.
This wasn't a legal issue, but just general confusion.

When people first hear about the project or see it posted they wouldn't
realize it was talking about a CLI tool.

It was a bit difficult to search for on Google but also places like
Twitter and in Slack searches and things.
This was the top complaint about rtx and many people were fairly outspoken
about disliking the name for this reason.

rtx was supposed to be a working title that I intended to change but
never got around to doing.
This change should've happened earlier when there were fewer users and
I apologize for not having done that sooner knowing that this was likely
going to be necessary at some point.


Asdf backend
================

- https://mise.jdx.dev/dev-tools/backends/asdf.html#asdf-backend
- https://mise.jdx.dev/dev-tools/backends/asdf.html

asdf is the original backend for mise.

It's the default if no backend is specified, e.g.: mise i ripgrep will
use asdf but mise i cargo:ripgrep will use the cargo backend.

You can explicitly specify the asdf backend with mise i asdf:ripgrep.
If you wish.

There are hundreds of plugins available in the mise registry and you can
also install plugins from git repos or local directories.

Comparison to asdf
=====================

- https://mise.jdx.dev/dev-tools/comparison-to-asdf.html#comparison-to-asdf

mise can be used as a drop-in replacement for asdf.

It supports the same .tool-versions files that you may have used with asdf
and uses asdf plugins.

It will not, however, reuse existing asdf directories (so you'll need to
either reinstall them or move them), and 100% compatibility is not a design goal.

**Casual users coming from asdf have generally found mise to just be a
faster, easier to use asdf**.


.. _direnv_replacement:

direnv replacement ? by Juan Luis
===================================

- https://social.juanlu.space/@astrojuanlu/111726680692340823
- https://mise.jdx.dev/direnv.html

@humitos Not many differences, mise is modeled after asdf.

Now it added environment management (like direnv) and tasks (like GNU Make),
so it's a more comprehensive solution.

I'm replacing direnv with mise as well. Not planning to use the tasks for now.


Python
========

- https://mise.jdx.dev/lang/python.html

Usage
-------

The following installs the latest version of python-3.11.x and makes it
the global default:


    mise use -g python@3.11
