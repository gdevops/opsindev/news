.. index::
   pair Opentofu; 1.6.0 (2024-01-10)

.. _opentofu_2024_01_10:

==============================================
2024-01-10 Opentofu 1.6.0 (2024-01-10)
==============================================

- https://github.com/opentofu/opentofu/releases/tag/v1.6.0
- https://opentofu.org/blog/opentofu-is-going-ga/

.. figure:: images/opentofu_1_6_0.webp

Official Announce on
====================

- https://opentofu.org/blog/opentofu-is-going-ga/
- https://opentofu.org/docs/cli/commands/test/
- https://opentofu.org/docs/language/settings/backends/s3/
- https://github.com/opentofu/registry
- https://opentofu.org/docs/intro/migration
- https://github.com/opentofu/opentofu/blob/v1.6/CHANGELOG.md
- https://opentofu.org/docs/intro/install/

Time for the big release! OpenTofu 1.6.0 is now stable!

Read more about this on our `blog ! <https://opentofu.org/blog/opentofu-is-going-ga/>`_

This version includes:

- the `new module testing feature <https://opentofu.org/docs/cli/commands/test/>`_,
- an `updated s3 backend <https://opentofu.org/docs/language/settings/backends/s3/>`_,
- tons of minor improvements, bug fixes, and performance improvements.

It also includes our `new registry <https://github.com/opentofu/registry>`_.

**This release is a drop-in replacement**, and you can follow our simple
migration guide to start using it with your existing infrastructure
configurations.

If you'd like to read about all the changes introduced, check out the
`detailed changelog <https://github.com/opentofu/opentofu/blob/v1.6/CHANGELOG.md>`_.

To get started, find the installation instructions `in our docs ! <https://opentofu.org/docs/intro/install/>`_

2024-01-10 OpenTofu is going GA
=====================================

- https://opentofu.org/blog/opentofu-is-going-ga/

Today is a big day for OpenTofu !

After four months of work, we're releasing the first stable release of
OpenTofu, a community-driven open source fork of Terraform.

**OpenTofu, a Linux Foundation project, is now production-ready**.

It’s a drop-in replacement for Terraform, and you can easily migrate
to it by following our `migration guide <https://opentofu.org/docs/intro/migration>`_.

Roni Frantchi wrote a great article prior to the holidays, describing
our road so far and up to the release candidate.

It’s an excellent resource to learn more about how we got to where we are now.

I’ll be focusing on the now, and what we are up to in the near future.
New Features


2023-12-19 OpenTofu Release Candidate Is Out, GA Set for Jan 10th
=======================================================================

- https://opentofu.org/blog/opentofu-release-candidate-is-out/
- https://github.com/opentofu/opentofu/issues/741

OpenTofu v1.6.0-rc1, the final stage before the first stable release,
is out today.

It follows the quick succession of its alpha and beta versions, on the
road to an expected General Availability release on January 10, 2024,
right after the holidays.

This release includes bug fixes, stability improvements, and updates to
documentation.

Importantly, this version highlights the stability of our new public registry,
which debuted with the v1.6.0-beta1 release three weeks ago, and has been
extensively tested by us and the early adopters from the OpenTofu community.


.. _robert_2024_01_12:

2024-01-12 **Sortie d'OpenTofu 1.6** de Stéphane Robert
=========================================================

- https://blog.stephane-robert.info/post/opentofu/

Conclusion
----------------

Information importante : Ce projet dispose du support de Partenaires
Technologiques Importants : Des entreprises telles que CloudFlare,
BuildKite, GitLab et Oracle soutiennent le projet.

Avec sa compatibilité avec Terraform, son engagement envers l'open-source,
et ses nouvelles fonctionnalités telles que tofu test en font une option
viable à qui cherche une alternative à Terraform.

Mais rien de mieux que le tester en condition réelle pour vérifier les
promesses annoncées par l'équipe d'OpenTofu.
C'est ce que je vais faire dès maintenant et je publierai mes conclusions
définitives dans les prochaines semaines.


Migration
------------

- https://blog.stephane-robert.info/post/opentofu/#migration-de-terraform-vers-opentofu
- https://opentofu.org/docs/intro/migration

2024-01-10 **OpenTofu Announces General Availability** by The Linux Foundation
==================================================================================

- https://www.linuxfoundation.org/press/opentofu-announces-general-availability
- https://fosstodon.org/@josh412/111761003161222718


SAN FRANCISCO – JANUARY 10, 2024 – The OpenTofu community is excited to
announce the general availability of OpenTofu, the open source fork of
Terraform™, now a production-ready project under the Linux Foundation.

This milestone follows four months of development from over five dozen
developers, offering a straightforward migration path for Terraform users.

The journey to this release highlights OpenTofu’s community-driven approach,
and the value of open source.

Two examples of this stand out:

- An RFC for client-side state encryption was submitted by a community
  member that tried to bring it to Terraform since 2016
- Multiple RFCs for the OpenTofu registry were submitted, leading to an
  architecture that is 10x faster and 10x cheaper to operate.
