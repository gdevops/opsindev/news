.. index::
   pair: docker ; local PostgreSQL database

.. _postgresql_2024_01_23:

========================================================================================
2024-01-23 **Connect to locally hosted postgresql from a docker container**
========================================================================================

- https://dev.to/21toffy/connect-to-locally-hosted-postgresql-from-a-docker-container-109o


Introduction
==============

If you are running a postgreSQL database on your host machine and want to
connect it to a Docker container running on the same host machine, there are a
few steps you need to follow.

In this article, we will go through the process of connecting a Docker
container running on a host machine to a database running on the same
host machine.

Prerequisite
==================

Before we start, make sure you have the following:

- Docker installed on your host machine
- A database running on your host machine
- Basic knowledge of Docker and databases

I write the article using the following
==================================================

- OS: Debian 11 (bullseye)
- PostgreSQL: 15 (Hosted on Debian)
- Docker: Server Application (which connects to PostgreSQL)

I am using docker-compose.yml to build application.

STEP 1
============

Please add host.docker.internal:

    version: '3' services:
      bank-server:
        ...  depends_on:
          ....
        restart: on-failure ports:
          - 9090:9090
        extra_hosts:
          - "host.docker.internal:<docker0 IP>"

To find IP of docker i.e. you can use::

    $> ifconfig docker0

::

    docker0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
            inet 172.17.0.1  netmask 255.255.0.0  broadcast 172.17.255.255

OR

::

    $> ip a | grep docker0

    1: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state
    DOWN group default
        inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
           valid_lft forever preferred_lft forever

STEP 2
==========

Update the listen_addresses setting in **postgresql.conf**.

This will allow PostgreSQL to listen on all available network interfaces.

In postgresql.conf, change listen_addresses to listen_addresses = '*'. Use
the command bellow to edit postgres conf

::

    sudo nano /etc/postgresql/<your_postgres_version>/main/postgresql.conf


Step 3
==========

Add a new entry to pg_hba.conf

In the pg_hba.conf file, add a new entry that allows connections from all
IP addresses.

::

    host    all             all             0.0.0.0/0               md5

Step 4
===========

Restart PostgreSQL

After making the changes, restart the PostgreSQL service using the following
command:

::

    sudo service postgresql restart


STEP 5
===========

Use host.docker.internal hostname to connect database from Server Application.

Ex: jdbc:postgresql://host.docker.internal:5432/bankDB

If you are encountering issues with the host.docker.internal hostname not being
recognized on Ubuntu, you can try replacing it with the local Docker IP address,
which is typically 172.17.0.1. This should allow your container to connect to
the database running on the host machine.

It's always a good idea to restrict access to your database to only the
necessary IP addresses for security purposes. Using the Docker bridge IP as
the allowed IP address can help ensure that only the container connected to
the same network can access the database.

you can use the docker bridge IP (use ip -h -c a to find that IP)

There may be other solutions to this problem, but this solution has been tested
and verified to work with The technologies and their respective versions I
highlighted above.

Thanks 😀.


Testé avec succès le mardi 23 janvier 2024 😀
==============================================

- Debian 12 bookworm
- Postgresql 15
- docker-compose
