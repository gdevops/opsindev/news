
.. index::
   pair: valkey ; redis
   ! valkey


.. _valkey_2024_03_28:

========================================================================
2024-03-28 **Linux Foundation Launches Open Source Valkey Community**
========================================================================

- https://www.linuxfoundation.org/press/linux-foundation-launches-open-source-valkey-community
- https://redis.com/blog/redis-adopts-dual-source-available-licensing/
- https://github.com/valkey-io/valkey


Announce
=============

SAN FRANCISCO – MARCH 28, 2024 – Today, the Linux Foundation announced
its intent to form Valkey, an open source alternative to the Redis in-memory,
NoSQL data store. Project contributors quickly gathered maintainer, community,
and corporate support to regroup in response to the recent license change
announced by Redis Inc. Valkey will continue development on Redis 7.2.4 and
will keep the project available for use and distribution under the open source
Berkeley Software Distribution (BSD) 3-clause license.

Since the Redis project was founded in 2009, thousands of open source developers
have contributed significantly to its growth and success. Many more developers
use it for caching, as well as a lower latency, higher throughput data store
alternative to their backend database, for real-time data analysis, session
store, message broker, and many other use cases. Developers ranked Redis the
sixth most used database in the 2023 Stack Overflow developer survey, and it
was among the top three most admired.

To continue improving on this important technology and allow for unfettered
distribution of the project, the community created Valkey, an open source high
performance key-value store. Valkey supports the Linux, macOS, OpenBSD, NetBSD,
and FreeBSD platforms. In addition, the community will continue working on its
existing roadmap including new features such as a more reliable slot migration,
dramatic scalability and stability improvements to the clustering system,
multi-threaded performance improvements, triggers, new commands, vector search
support, and more.

Industry participants, including Amazon Web Services (AWS), Google Cloud,
Oracle, Ericsson, and Snap Inc. are supporting Valkey. They are focused on
making contributions that support the long-term health and viability of the
project so that everyone can benefit from it.


Github project A new project to resume development on the formerly open-source Redis project. We're calling it Valkey, like a Valkyrie.
==========================================================================================================================================

- https://github.com/valkey-io/valkey



Redis Adopts Dual Source-Available Licensing
==================================================

- https://redis.com/blog/redis-adopts-dual-source-available-licensing/
- https://redis.com/legal/rsalv2-agreement/
- https://redis.com/legal/server-side-public-license-sspl/

Future Redis releases will continue to offer free and permissive use of
the source code under dual RSALv2 and SSPLv1 licenses; these releases will
combine advanced data types and processing engines previously only available
in Redis Stack.

The
Beginning today, all future versions of Redis will be released with
source-available licenses. Starting with Redis 7.4, Redis will be dual-licensed
under the Redis Source Available License (RSALv2) and Server Side Public
License (SSPLv1). Consequently, Redis will no longer be distributed under the
three-clause Berkeley Software Distribution (BSD).



