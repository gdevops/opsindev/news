

==================================================
2024-03-07 **Opentofu in the CNCF landscape**
==================================================

- :ref:`sysops:opentofu`
- https://landscape.cncf.io/guide?item=provisioning--automation-configuration--opentofu#provisioning--automation-configuration
- https://github.com/cncf/landscape/commit/889700c406a87d6a44b657a367eac8dfb0c658bc

.. figure:: images/opentofu_cncf.webp
