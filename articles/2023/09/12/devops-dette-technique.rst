.. index::
   pair:Snowflakes; Servers
   ! Snowflakes servers
   ! Dette technique

.. _devops_2023_09_12:

===========================================================================
2023-09-12 Devops - Gestion de la dette technique de l'infra as code
===========================================================================

- https://blog.stephane-robert.info/post/devops-dette-technique/
- https://blog.eleven-labs.com/fr/comment-creer-de-la-dette-technique-des-le-debut-d-un-nouveau-projet/


Introduction
===============

La lecture de ce `billet du blog d’eleven labs <https://blog.eleven-labs.com/fr/comment-creer-de-la-dette-technique-des-le-debut-d-un-nouveau-projet/>`_ m’a donné l’idée de le
décliner sur le développement à base de code d’infrastructure (Ansible,
Terraform, Puppet, Pulumi, Crossplane …).

Pourquoi ? Parce que je vois trop souvent mis de côté au nom de l’agilité,
de la rapidité et de la simplicité de mise en œuvre tout un ensemble de
bonnes pratiques.

Dans un premier temps, je vais définir ce qu’est une dette technique, puis
énumérer les différents types de dettes, et pour en finir certaines causes

Qu’est qu’une dette technique ?
=======================================

Je vous propose de reprendre simplement la définition de Wikipédia :

La dette technique est un concept du développement logiciel inventé par
[Ward Cunningham](https://fr.wikipedia.org/wiki/Ward_Cunningham) en 1992.

Le terme vient d'une métaphore, inspirée du concept existant de dette dans
le domaine des finances et des entreprises, appliquée au domaine du
développement logiciel.

Elle désigne la difficulté à faire évoluer et à corriger un code source
qui a été mal conçu initialement.

Pour rappel, l’utilisation des langages ne se limitent plus à la seule
création de programmes informatiques.
Des langages ont été créés pour instancier des ressources virtuelles
d’infrastructure informatique.

Ces langages ne se limitent pas à la seule création de machines virtuelles,
car on peut désormais instancier beaucoup de type de ressources dont les
équipements de réseau et de stockage.

On peut ainsi créer des infrastructures complexes en peu de temps.

Ici, la dette technique fait référence aux décisions de conception qui
viennent compromettre l’intégrité, la flexibilité ou la maintenabilité
à long terme de celles-ci.

Si rien n’est fait, cette dette peut s’accumuler au fil du temps, ce qui
rend les changements et leur gestion de plus en plus complexes, nécessitant
ainsi un investissement important en temps et en ressources.


Comment créer de la dette technique sur des projets d’infrastructure ?
===========================================================================

Difficultés de recrutement

Ces dernières années les entreprises sont en tension dans le domaine du
recrutement.

Trouver des personnes compétentes est assez compliqué.

Quand je parle de compétences, je ne parle pas que de compétences techniques,
mais aussi des savoir-faire. On ne remplacera jamais l’expérience et la pratique.

La vraisemblance de simplicité et de rapidité de création d’infrastructures
avec ces langages introduisent une nouvelle dimension de complexité et
de risque, ouvrant la porte à une « nouvelle forme dette technique ».

La prolifération des tutoriels, du code accessible et généré par l’IA a
transformé le paysage du développement via l’infrastructure en tant que
code.

**On ne se rend pas compte que l’utilisation d’un module Terraform ou d’un
rôle Ansible trouvé sur internet sans en comprendre pleinement son fonctionnement,
peut se révéler dangereux en termes de sécurité, de performances et de
résilience**.

Je ne parle même pas de l’IA.

L’absence de véritables professionnels
==============================================

Par un choix délibéré de ne pas recourir à des spécialistes de l’architecture
et de la sécurité, on peut produire de lourdes dettes techniques.

Ces professionnels sont indispensables, car en leur absence de mauvaises
décisions peuvent être prises.

Et ce genre d’erreur dès le début du projet peuvent conduire à une véritable
“Bérézina”. Le fait de déployer une application sur un cluster Kubernetes
ne fait pas de vous un expert de ce genre de solutions !


Des mauvais choix techniques
=====================================

Une autre erreur : vouloir utiliser une technologie naissante ou inconnue.

Ce genre de choix est souvent fait pour suivre la tendance, pour apprendre
une nouvelle technologie, pour essayer de répondre à un besoin.

Quelle que soit la source de votre décision, il ne faut pas oublier que
vous mettez les pieds en terre inconnue.
Même si vous avez assisté à une démo qui déchire, il ne faut pas oublier
que vous devrez ensuite être capable de l’exploiter en conditions réelles.

Il faudra assurer la montée en compétence de toutes vos équipes pour
assurer le maintien en condition opérationnel.

Pour éviter ce genre d’erreur, essayer de répondre à ces questions :

- Depuis combien de temps existe ce produit ?
  J’ai remarqué qu’il faut attendre 2 à 3 ans pour une technologie commence à murir.
- Cet outil est-il utilisé par à un grand nombre de clients ?
- Trouve-t-on facilement des ressources ? Un outil largement documenté
  et cité vous évitera de longues recherches.
- Combien de personnes assurent son développement ?
  Une fondation est plus sûre qu’un seul développeur.
- Ai-je assez de profils connaissant cette techno ?
- Des solutions plus classiques ?

.. _snowflakes_servers:

Le manque de cohérence
==========================

- https://martinfowler.com/bliki/SnowflakeServer.html

Le manque d’automatisation et le recours à des solutions trouvées sur
internet peuvent conduire à la création de serveurs dits uniques connus
sous le nom de `snowflakes servers <https://martinfowler.com/bliki/SnowflakeServer.html>`_

Posséder plusieurs serveurs de ce type contribuent à augmenter la dette
technique.

La gestion, la mise à jour et le dépannage de ces serveurs nécessitent
des connaissances spécialisées et des efforts manuels.

**Les incohérences entre les serveurs peuvent entraîner des comportements
inattendus, compliquer la reprise après incident et ralentir tout le
processus de livraison**

Procédurale contre déclaratif
======================================

L’utilisation de langages utilisant une approche procédurale qui décrit
le “comment” atteindre un état souhaité.

Une autre approche, appelée déclarative, ne fait que décrire l’état final
souhaité.

Je ne vais pas revenir sur ce débat. Mais il faut privilégier les langages
déclaratifs.

Construire des usines à gaz
================================

Un autre moyen de créer de la dette technique est de transformer le code
en une véritable usine à gaz.

**C’est par exemple transformer un langage déclaratif comme Terraform ou
Ansible en langage procédural**.

Cela est dû à l’introduction de structures de contrôle telles que les
boucles et les instructions conditionnelles, qui déforment la nature
« déclarative » inhérente à ces langages.

**Les langages déclaratifs sont conçus pour assurer un résultat prévisible**.

L’introduction de ces éléments peut conduire à des résultats différents
dans différentes conditions, ce qui rend plus difficile la fourniture
d’un état d’infrastructure idem-potent.

Ces techniques augmentent la complexité de la base de code, ce qui la
rend plus difficile à comprendre et à maintenir.

Un seul conseil, **écrivez des petits programmes se limitant à des
objectifs limités**.

Le fire and forget
========================

Lorsque l’IaC est utilisé uniquement pour le provisionnement, puis laissé
au placard, peut conduire à créer de la dette technique.

Toutes modifications apportées à l’infrastructure de manière manuelle,
que ce soit pour résoudre des problèmes ou ajuster des configurations,
ne sont que très rarement reporté dans le code IaC.

Cet écart crée un écart toujours plus grand entre le code et l’infrastructure
réelle, qui se transforme en une dette technique importante.

La dette devient apparente lorsqu’il est nécessaire de reproduire l’infrastructure
ou quand une défaillance nécessite une reconstruction.

La base de code IaC ne représentera pas avec précision l’infrastructure
fonctionnelle, ce qui entraînera des incidents.

Négliger le besoin de support et de maintenance du code
===========================================================

Comme tout autre logiciel, le code IaC n’est pas à l’abri des effets du
temps, et négliger son support peut conduire à l’accumulation de dette
technique.

Les outils IaC et les services cloud qu’ils gèrent sont régulièrement
mis à jour avec de nouvelles fonctionnalités, des améliorations et parfois
même des correctifs.
Ces mises à jour peuvent rendre le code IaC existant obsolète ou moins
optimal.

Une maintenance régulière de la base de code IaC est nécessaire pour
suivre ces changements, faute de quoi cela peut entraîner à de la dette technique.

À mesure que les cybermenaces et les réglementations de conformité évoluent,
le code IaC qui était autrefois considéré comme sécurisé peut ne plus l’être.

Des tests et des mises à jour régulières sont nécessaires pour s’assurer
que l’infrastructure reste sûre et conforme.

Eviter la dette technique
=============================

- Étudiez minutieusement tous les modules ou rôles que vous avez récupérés
  sur internet avant de les intégrer dans votre base de code.
  Identifiez leurs impacts possibles et les risques de sécurité potentiels.


Soyez prudent lorsque vous utilisez du code généré par de l’IA.
Il est important de s’assurer qu’elle s’aligne sur vos normes et vos
meilleures pratiques.

- Adoptez l’approche déclarative : Formez vos équipes à penser de manière
  abstraite et à s’adapter à l’approche « non interventionniste » du déclaratif.

- Évitez les usines à Gaz !!!

- Mettez à jour régulièrement votre code pour garantir le fonctionnement
  avec les nouvelles versions des outils et des services cloud.

- **Exigez la documentation**.
  La tenue d’une documentation complète et à jour est un aspect souvent
  négligé,mais essentiel pour limiter la dette technique.

  Il permet une meilleure compréhension, un meilleur support et facilite
  le processus d’intégration pour les nouveaux membres dans votre équipe.

- Automatisez les tests : Implémentez des tests automatisés pour, vous
  aidez à détecter rapidement les problèmes potentiels.


Et …
============

J’ai essayé de produire un document complet, peut-être trop ou pas assez.
Je fais appel à vos avis et conseils pour l’améliorer. Merci d’avance.

- https://blog.stephane-robert.info/post/devops-dette-technique/
