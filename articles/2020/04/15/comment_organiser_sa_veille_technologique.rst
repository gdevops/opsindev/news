.. index::
   pair: Veille technologique ; Blogs
   pair: Veille technologique ; Compagnons deu devops
   ! Veille technologique

.. _devops_veille_technologique:

==============================================
Comment organiser sa veille technologique ?
==============================================

.. seealso::

   - https://lydra.fr/rdo-4-comment-organiser-sa-veille-technologique/

.. contents::
   :depth: 3

Les liens
===========

L’avenir de Docker swarm
---------------------------

- https://devclass.com/2020/02/25/mirantis-to-keep-docker-swarm-buzzing-around-pledges-new-features
  Le dépôt Github : https://github.com/docker/classicswarm

DNS sur HTTPS
---------------

- https://web.developpez.com/actu/294911/Firefox-active-par-defaut-le-DNS-over-HTTPS-pour-ses-utilisateurs-aux-Etats-Unis-et-donne-la-marche-a-suivre-pour-les-utilisateurs-dans-d-autres-regions-du-monde

La fuite de donnée de Décathlon
---------------------------------

- https://siecledigital.fr/2020/02/25/decathlon-une-fuite-laisse-123-millions-de-donnees-dans-la-nature

Échanger avec nos pairs
========================

- La communauté des Compagnons du DevOps : https://www.compagnons-devops.fr/
- https://stackoverflow.com/

Les blogs
==========

- https://about.gitlab.com/blog/
- https://blog.wescale.fr/
- https://blog.d2si.io/
- https://www.ovh.com/blog/
- https://lydra.fr/blog/

Les agregateurs de contenue pour la veille
============================================

- http://news.humancoders.com/
- https://www.journalduhacker.net/
- Hacker News : https://news.ycombinator.com/
- https://www.reddit.com/ (exemples sub : r/devops
  r/sre
  r/gcp
  r/aws
  r/ovh
  r/git
  r/gitlab
  r/k8s
  r/serverless
  r/docker
  r/cloudcomputing
  r/terraform
  r/ansible
  r/debian
  r/linux
  r/linuxadmin
  r/opensource)
- Guriosity (une newsletter hebdo sur les meilleurs blog-posts des startups françaises) : https://guriosity.com/

Les outils
============

- Lecteur de flux RSS pour les articles non qualifiés : https://www.freshrss.org/
- Gestionnaire de marques pages wallabag pour les articles qualifiés :http://wallabag.org/
- Base de notes partagés pour le contenu : https://joplinapp.org/

Les chaînes YouTube
====================

- Xavki : https://www.youtube.com/channel/UCs_AZuYXi6NA9tkdbhjItHQ
- Cocadmin : https://www.youtube.com/channel/UCVRJ6D343dX-x730MRP8tNw
- Les Compagnons du DevOps : https://www.youtube.com/channel/UCauIDghddUNu6Fto1nR9Bmg

Les Podcasts
===============

- Radio DevOps : https://lydra.fr/radio-devops/
- Dev’Obs : https://p7t.tech/
- Electro Monkeys : https://electro-monkeys.fr/
- If This Then Dev : https://ifttd.io/
- Artisan Développeur : https://artisandeveloppeur.fr/podcast/

Les conventions
================

- https://www.devopsrex.fr/
- http://www.devops-dday.com/
- https://devopsdays.org/
- https://www.devoxx.fr/
- https://fosdem.org/

Se former
===========

- OpenClassRooms : https://openclassrooms.com/fr/
- Udemy : https://www.udemy.com/
- Fun Mooc : https://www.fun-mooc.fr/
- Grafikart : https://www.grafikart.fr/
- Linux Foundation : https://training.linuxfoundation.org/
- Kubernetes Training and Certification : https://kubernetes.io/training/
- Guide d’apprentissage : https://github.com/Thialala/software-engineering-learning-guide
